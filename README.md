# Sample Code App #

A sample "banking" application that uses encryption and aims to demonstrate clean software architecture, using what the Android architecture components have to offer, Kotlin Coroutines and other libraries.

## About this app ##

### Dummy functionalities include:

* User login (then app will create and encrypt a database)
* Get a list with bank accounts (for such user)
* Navigate into history of account transactions
* Deposit/Withdraw to/from savings account

### Architecture Components in this development sample:

* Navigation Component
* ViewModel
* LiveData
* Room (+ SQLCipher)
* Paging
* Android KTX
* LifeCycles
 
### This application intends to implement Clean Architecture so it's been arranged by feature.
#### Features:
* main
* login
* users
* transactions
* storage
 - cache
 - database
* backend
* auth
* accounts
* common

### NOTE:
* To log-in use the hard-coded pin (0000). The user pin isn't propagated to the sql open helper factory, just because it was easier to do. However, the database will indeed be created and encrypted when you login the first time. To make it so the app respects user's input.
* Navigation drawer items do not have a behavior besides the "Log out".
* Backend is completely mocked. 