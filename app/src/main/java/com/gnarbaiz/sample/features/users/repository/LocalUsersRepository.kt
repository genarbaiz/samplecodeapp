package com.gnarbaiz.sample.features.users.repository

import com.gnarbaiz.sample.features.users.vo.User

/**
 * Created by Gonzalo Narbaiz on 17-4-21.
 */

interface LocalUsersRepository {
    suspend fun getLoggedInUser(): User?
    suspend fun createUser(user: User)
    suspend fun login(user: User)
    suspend fun deleteUserSettings()
}

