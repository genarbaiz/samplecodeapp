package com.gnarbaiz.sample.features.transactions.di

import com.gnarbaiz.sample.R
import com.gnarbaiz.sample.features.accounts.vo.Account
import com.gnarbaiz.sample.features.storage.database.AppDatabase
import com.gnarbaiz.sample.features.transactions.repository.LocalTransactionsRepository
import com.gnarbaiz.sample.features.transactions.repository.RemoteTransactionsRepository
import com.gnarbaiz.sample.features.transactions.repository.internal.LocalTransactionsRepositoryImpl
import com.gnarbaiz.sample.features.transactions.repository.internal.RemoteTransactionsRepositoryImpl
import com.gnarbaiz.sample.features.transactions.ui.AddTransactionUseCase
import com.gnarbaiz.sample.features.transactions.ui.AddTransactionViewModel
import com.gnarbaiz.sample.features.transactions.ui.GetAvailableAccountsForTransactionUseCase
import com.gnarbaiz.sample.features.transactions.ui.TransactionsViewModel
import com.gnarbaiz.sample.features.transactions.usecase.RefreshAccountTransactionsUseCase
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Gonzalo Narbaiz on 17-4-21
 */

val transactionsModule = module {
    viewModel { (account: Account) -> TransactionsViewModel(get(), get(), account) } // Assisted injection
    viewModel { (account: Account) -> AddTransactionViewModel(account, get(), get()) } // Assisted injection
    single { LocalTransactionsRepositoryImpl(get()) as LocalTransactionsRepository }
    single { RemoteTransactionsRepositoryImpl(get(), get()) as RemoteTransactionsRepository }
    single { get<AppDatabase>().transactionDao() }
    single {
        AddTransactionUseCase(
            androidContext().getString(R.string.deposit),
            androidContext().getString(R.string.withdraw),
            get(),
            get(),
            get(),
            get()
        )
    }
    factory { RefreshAccountTransactionsUseCase(get(), get()) }
    factory { GetAvailableAccountsForTransactionUseCase(get(), get()) }
}