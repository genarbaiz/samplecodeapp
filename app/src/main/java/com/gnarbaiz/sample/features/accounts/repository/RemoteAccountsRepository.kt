package com.gnarbaiz.sample.features.accounts.repository

import com.gnarbaiz.sample.features.accounts.vo.Account
import com.gnarbaiz.sample.features.users.vo.User
import java.math.BigDecimal

/**
 * Repository that works with a remote data source, which exposes domain specific classes, not DTOs.
 */
interface RemoteAccountsRepository {
    suspend fun fetchAccounts(user: User): List<Account>
    suspend fun fetchAccountBalance(accountId: Int): BigDecimal
    suspend fun updateAccountBalance(accountId: Int, newBalance: BigDecimal)
}