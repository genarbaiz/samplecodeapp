package com.gnarbaiz.sample.features.users.di

import com.gnarbaiz.sample.features.storage.database.AppDatabase
import com.gnarbaiz.sample.features.users.repository.LocalUsersRepository
import com.gnarbaiz.sample.features.users.repository.RemoteUserRepository
import com.gnarbaiz.sample.features.users.repository.internal.LocalUsersRepositoryImpl
import com.gnarbaiz.sample.features.users.repository.internal.RemoteUserRepositoryImpl
import org.koin.dsl.module

/**
 * Created by Gonzalo Narbaiz on 17-4-21
 */

val usersModule = module {
    single { get<AppDatabase>().userDao() }
    single { RemoteUserRepositoryImpl(get()) as RemoteUserRepository }
    single { LocalUsersRepositoryImpl(get()) as LocalUsersRepository }
}