package com.gnarbaiz.sample.features.users.repository.internal

import com.gnarbaiz.sample.features.users.dto.toModel
import com.gnarbaiz.sample.features.auth.AuthService
import com.gnarbaiz.sample.features.auth.vo.Token
import com.gnarbaiz.sample.features.backend.BackendApi
import com.gnarbaiz.sample.features.users.repository.RemoteUserRepository
import com.gnarbaiz.sample.features.users.vo.User

class RemoteUserRepositoryImpl(private val backend: BackendApi) : RemoteUserRepository,
    AuthService {
    override suspend fun getUserToken(appToken: String): Token = backend.fetchUserToken(appToken)
    override suspend fun getPerson(token: String): User = backend.fetchPerson(token).toModel()
}