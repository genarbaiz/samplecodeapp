package com.gnarbaiz.sample.features.accounts.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.gnarbaiz.sample.features.accounts.repository.LocalAccountsRepository
import com.gnarbaiz.sample.features.accounts.repository.RemoteAccountsRepository
import com.gnarbaiz.sample.features.accounts.vo.Account
import com.gnarbaiz.sample.features.common.usecase.UseCase
import com.gnarbaiz.sample.features.common.vo.catchResult
import com.gnarbaiz.sample.features.common.vo.onFailure
import com.gnarbaiz.sample.features.users.repository.LocalUsersRepository

class FetchUserAccountsUseCase(
    private val localUsersRepository: LocalUsersRepository,
    private val localAccountsRepository: LocalAccountsRepository,
    private val remoteAccountsRepository: RemoteAccountsRepository
) : UseCase<Unit, LiveData<List<Account>>> {
    override suspend fun invoke(param: Unit): LiveData<List<Account>> {
        val user = localUsersRepository.getLoggedInUser()!!

        return liveData {
            catchResult {
                // Fetch from network
                val accounts = remoteAccountsRepository.fetchAccounts(user)

                // Emit right away
                emit(accounts)

                if (accounts.isNotEmpty()) {
                    // Store to disk
                    localAccountsRepository.storeAccounts(user, accounts)
                } else {
                    localAccountsRepository.deleteAccounts(user)
                }
            }.onFailure {
                // Log
                it.printStackTrace()
            }

            emitSource(localAccountsRepository.getAccountsRefreshing(user))
        }
    }
}