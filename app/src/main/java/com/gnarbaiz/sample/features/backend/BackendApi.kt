package com.gnarbaiz.sample.features.backend

import com.gnarbaiz.sample.features.accounts.dto.AccountDTO
import com.gnarbaiz.sample.features.accounts.vo.Account
import com.gnarbaiz.sample.features.auth.vo.Token
import com.gnarbaiz.sample.features.transactions.dto.TransactionDTO
import com.gnarbaiz.sample.features.users.dto.UserDTO
import com.gnarbaiz.sample.features.users.vo.User
import java.math.BigDecimal

interface BackendApi {
    suspend fun fetchUserToken(username: String): Token
    suspend fun fetchPerson(token: String): UserDTO
    suspend fun fetchTransactions(userAccessToken: String, accountId: Int): List<TransactionDTO>
    suspend fun addTransaction(userAccessToken: String, transaction: TransactionDTO)
    suspend fun fetchAccounts(userAccessToken: String, user: User): List<AccountDTO>
    suspend fun fetchAccountBalance(userAccessToken: String, accountId: Int): BigDecimal
    suspend fun updateAccountBalance(userAccessToken: String, accountId: Int, newBalance: BigDecimal)
}