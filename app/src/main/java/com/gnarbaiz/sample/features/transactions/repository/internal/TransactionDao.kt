package com.gnarbaiz.sample.features.transactions.repository.internal

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gnarbaiz.sample.features.transactions.entity.TransactionEntity
import com.gnarbaiz.sample.features.transactions.vo.Transaction

/**
 * Created by Gonzalo Narbaiz on 17-4-21.
 */

@Dao
interface TransactionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTransactions(transactions: List<TransactionEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTransaction(transaction: TransactionEntity)

    // Since dates are not formatted properly, ordering doesn't really work as it should.
    @Query("SELECT * FROM ${TransactionEntity.TABLE_NAME} WHERE accountId=:accountId ORDER BY date DESC")
    fun getTransactions(accountId: Int): DataSource.Factory<Int, TransactionEntity>

    @Query("DELETE FROM ${TransactionEntity.TABLE_NAME} WHERE accountId=:accountId")
    suspend fun deleteTransactions(accountId: Int)
}