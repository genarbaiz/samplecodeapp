package com.gnarbaiz.sample.features.storage.cache

interface Fetcher<Key : Any, Value : Any> : Cache<Key, Value> {
    override suspend fun evict(key: Key) {
        // No-op
    }

    override suspend fun evictAll() {
        // No-op
    }

    override suspend fun set(key: Key, value: Value) {
        // No-op
    }
}