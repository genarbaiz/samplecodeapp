package com.gnarbaiz.sample.features.transactions.repository

import com.gnarbaiz.sample.features.transactions.vo.Transaction


interface RemoteTransactionsRepository {
    suspend fun getTransactions(accountId: Int): List<Transaction>
    suspend fun addTransaction(transaction: Transaction)
}