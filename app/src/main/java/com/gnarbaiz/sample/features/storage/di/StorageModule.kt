package com.gnarbaiz.sample.features.storage.di

import com.commonsware.cwac.saferoom.SafeHelperFactory
import com.gnarbaiz.sample.features.auth.di.USER_PIN
import com.gnarbaiz.sample.features.storage.database.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/**
 * Created by Gonzalo Narbaiz on 17-4-21
 */

val storageModule = module {
    single { AppDatabase.getInstance(androidContext(), get()) }
    // TODO: Assisted injection.
    single { SafeHelperFactory(USER_PIN) }
}