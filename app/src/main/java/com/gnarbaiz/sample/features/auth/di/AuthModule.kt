package com.gnarbaiz.sample.features.auth.di

import com.gnarbaiz.sample.features.auth.AuthService
import com.gnarbaiz.sample.features.auth.internal.AuthServiceImpl
import org.koin.dsl.module
import java.util.*

/**
 * Created by Gonzalo Narbaiz on 17-4-21
 */

// FIXME: User input should either be propagated to the open helper factory
// Don't judge for the hardcoded pin, please! :)
val USER_PIN = charArrayOf('0', '0', '0', '0')
// Some app secret that we shouldn't normally keep in the app and definitely not hard coded in the code :)
val APP_TOKEN = UUID.randomUUID().toString()

val authModule = module {
    single { AuthServiceImpl(get()) as AuthService }
}