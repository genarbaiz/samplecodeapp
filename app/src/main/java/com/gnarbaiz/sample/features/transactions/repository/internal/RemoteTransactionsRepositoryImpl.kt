package com.gnarbaiz.sample.features.transactions.repository.internal

import com.gnarbaiz.sample.features.auth.di.APP_TOKEN
import com.gnarbaiz.sample.features.transactions.dto.toDTO
import com.gnarbaiz.sample.features.transactions.dto.toDomainModel
import com.gnarbaiz.sample.features.transactions.repository.RemoteTransactionsRepository
import com.gnarbaiz.sample.features.transactions.vo.Transaction
import com.gnarbaiz.sample.features.auth.AuthService
import com.gnarbaiz.sample.features.backend.BackendApi

class RemoteTransactionsRepositoryImpl(
    private val backendApi: BackendApi,
    private val authService: AuthService
) :
    RemoteTransactionsRepository {

    override suspend fun getTransactions(accountId: Int): List<Transaction> =
        backendApi.fetchTransactions(authService.getUserToken(APP_TOKEN).accessToken, accountId)
            .map { it.toDomainModel() }


    override suspend fun addTransaction(transaction: Transaction) {
        backendApi.addTransaction(authService.getUserToken(APP_TOKEN).accessToken, transaction.toDTO())
    }
}