package com.gnarbaiz.sample.features.accounts.usecase

import com.gnarbaiz.sample.features.accounts.repository.LocalAccountsRepository
import com.gnarbaiz.sample.features.accounts.repository.RemoteAccountsRepository
import com.gnarbaiz.sample.features.common.usecase.UseCase
import com.gnarbaiz.sample.features.common.vo.Result
import com.gnarbaiz.sample.features.common.vo.catchResult
import com.gnarbaiz.sample.features.common.vo.onFailure
import com.gnarbaiz.sample.features.users.repository.LocalUsersRepository

/**
 * Use case that will update the local repository with data from the remote.
 * If the use case does not error, then the operation has been successful.
 */
class RefreshUserAccountsUseCase(
    private val localUsersRepository: LocalUsersRepository,
    private val localAccountsRepository: LocalAccountsRepository,
    private val remoteAccountsRepository: RemoteAccountsRepository
) : UseCase<Unit, Result<Unit>> {
    override suspend fun invoke(param: Unit): Result<Unit> =
        catchResult {
            val user = localUsersRepository.getLoggedInUser()!!
            // Fetch from network
            val accounts = remoteAccountsRepository.fetchAccounts(user)
            if (accounts.isNotEmpty()) {
                // Store to disk
                localAccountsRepository.storeAccounts(user, accounts)
            } else {
                localAccountsRepository.deleteAccounts(user)
            }
        }.onFailure {
            // Log
            it.printStackTrace()
        }
}