package com.gnarbaiz.sample.features.users.usecase

import com.gnarbaiz.sample.features.common.usecase.UseCase
import com.gnarbaiz.sample.features.users.repository.LocalUsersRepository

class LogOutUserUseCase(private val localUsersRepository: LocalUsersRepository) :
    UseCase<Unit, Result<Unit>> {

    override suspend fun invoke(param: Unit) =
        runCatching { localUsersRepository.deleteUserSettings() }
}