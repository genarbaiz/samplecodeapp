package com.gnarbaiz.sample.features.users.vo

/**
 * Created by Gonzalo Narbaiz on 17-4-21.
 */

data class User(val id: Int = 0, val firstName: String, val lastName: String)


