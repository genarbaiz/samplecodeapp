package com.gnarbaiz.sample.features.transactions.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.gnarbaiz.sample.features.accounts.repository.LocalAccountsRepository
import com.gnarbaiz.sample.features.accounts.repository.RemoteAccountsRepository
import com.gnarbaiz.sample.features.common.usecase.UseCase
import java.math.BigDecimal

class GetAccountBalanceUseCase(
    private val localAccountsRepository: LocalAccountsRepository,
    private val remoteAccountsRepository: RemoteAccountsRepository
) :
    UseCase<Int, LiveData<BigDecimal>> {
    override suspend fun invoke(param: Int) =
        liveData {
            val actualBalance = remoteAccountsRepository.fetchAccountBalance(param)
            emit(actualBalance)
            localAccountsRepository.updateAccountBalance(param, actualBalance)
            emitSource(localAccountsRepository.getAccountBalanceRefreshing(param))
        }
}