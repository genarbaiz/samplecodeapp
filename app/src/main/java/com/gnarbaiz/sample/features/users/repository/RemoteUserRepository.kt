package com.gnarbaiz.sample.features.users.repository

import com.gnarbaiz.sample.features.auth.vo.Token
import com.gnarbaiz.sample.features.users.vo.User

interface RemoteUserRepository {
    suspend fun getUserToken(appToken: String): Token
    suspend fun getPerson(token: String): User
}