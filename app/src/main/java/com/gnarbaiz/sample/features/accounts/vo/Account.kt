package com.gnarbaiz.sample.features.accounts.vo

import com.gnarbaiz.sample.features.common.converter.BigDecimalSerializer
import kotlinx.serialization.Serializable
import java.math.BigDecimal

@Serializable
data class Account(
    val id: Int,
    val name: String,
    val iban: String,
    val type: String,
    val currency: String,
    @Serializable(with = BigDecimalSerializer::class)
    val balance: BigDecimal
): java.io.Serializable {
    companion object {
        // TODO We can have a sealed class. SavingsAccount and PaymentAccount
        const val TYPE_PAYMENT = "payment"
        const val TYPE_SAVINGS = "savings"
    }
}