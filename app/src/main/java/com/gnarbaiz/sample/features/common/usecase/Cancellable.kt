package com.gnarbaiz.sample.features.common.usecase

interface Cancellable {
    fun cancel()
}