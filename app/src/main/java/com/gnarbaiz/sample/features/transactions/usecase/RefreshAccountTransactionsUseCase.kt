package com.gnarbaiz.sample.features.transactions.usecase

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.gnarbaiz.sample.features.accounts.vo.Account
import com.gnarbaiz.sample.features.common.usecase.Cancellable
import com.gnarbaiz.sample.features.common.usecase.UseCase
import com.gnarbaiz.sample.features.common.usecase.UseCaseContextScope
import com.gnarbaiz.sample.features.common.vo.catchResult
import com.gnarbaiz.sample.features.common.vo.onFailure
import com.gnarbaiz.sample.features.transactions.repository.LocalTransactionsRepository
import com.gnarbaiz.sample.features.transactions.repository.RemoteTransactionsRepository
import com.gnarbaiz.sample.features.transactions.vo.Transaction
import kotlinx.coroutines.*

class RefreshAccountTransactionsUseCase(
    private val localTransactionsRepository: LocalTransactionsRepository,
    private val remoteTransactionsRepository: RemoteTransactionsRepository
) :
    UseCase<Account, LiveData<PagedList<Transaction>>>, Cancellable {
    private val internalScope
        get() =             UseCaseContextScope(SupervisorJob() + Dispatchers.Main)

    override suspend fun invoke(param: Account): LiveData<PagedList<Transaction>> {
        internalScope.launch {
            catchResult {
                val transactions = remoteTransactionsRepository.getTransactions(param.id)
                if (transactions.isNotEmpty()) {
                    localTransactionsRepository.addTransactions(transactions)
                } else {
                    localTransactionsRepository.deleteTransactions(param.id)
                }
            }.onFailure {
                // Log
                it.printStackTrace()
            }
        }
        return localTransactionsRepository.getTransactions(accountId = param.id)
    }

    override fun cancel() {
        internalScope.cancel()
    }
}