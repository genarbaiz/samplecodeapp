package com.gnarbaiz.sample.features.users.repository.internal

import com.gnarbaiz.sample.features.users.vo.User
import com.gnarbaiz.sample.features.users.entity.toEntity
import com.gnarbaiz.sample.features.users.repository.LocalUsersRepository

class LocalUsersRepositoryImpl(private val userDao: UserDao) :
    LocalUsersRepository {
    private var cachedUser: User? = null

    override suspend fun login(user: User) {
        cachedUser = user
    }

    override suspend fun getLoggedInUser(): User? {
        return cachedUser
    }

    override suspend fun createUser(user: User) {
        userDao.createUser(user.toEntity())
    }

    override suspend fun deleteUserSettings() {
        cachedUser = null
        // TODO: Remove token cache
    }
}