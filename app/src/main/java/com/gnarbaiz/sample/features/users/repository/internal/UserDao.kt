package com.gnarbaiz.sample.features.users.repository.internal

import androidx.room.*
import com.gnarbaiz.sample.features.users.entity.UserEntity

/**
 * Created by Gonzalo Narbaiz on 17-4-21.
 */

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun createUser(user: UserEntity)

    @Query("DELETE FROM ${UserEntity.TABLE_NAME} WHERE id=:id")
    suspend fun deleteUser(id: Int)
}