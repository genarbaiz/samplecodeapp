package com.gnarbaiz.sample.features.transactions.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.gnarbaiz.sample.features.transactions.vo.Transaction

interface LocalTransactionsRepository  {
    fun getTransactions(accountId: Int): LiveData<PagedList<Transaction>>
    suspend fun addTransaction(transaction: Transaction)
    suspend fun deleteTransactions(accountId: Int)
    suspend fun addTransactions(transactions: List<Transaction>)
}