package com.gnarbaiz.sample.features.common.usecase

import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.CoroutineContext

/**
 * Created by Gonzalo Narbaiz on 17-4-21.
 */

internal class UseCaseContextScope(context: CoroutineContext) : CoroutineScope {
    override val coroutineContext: CoroutineContext = context
}