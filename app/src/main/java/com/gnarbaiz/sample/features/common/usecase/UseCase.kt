package com.gnarbaiz.sample.features.common.usecase

/**
 * Created by Gonzalo Narbaiz on 17-4-21.
 */

interface UseCase<I : Any?, O : Any> {
    suspend operator fun invoke(param: I): O?
}

suspend operator fun <O : Any> UseCase<Unit, O>.invoke() = invoke(Unit)