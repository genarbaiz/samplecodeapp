package com.gnarbaiz.sample.features.backend.di

import com.gnarbaiz.sample.features.backend.BackendApi
import com.gnarbaiz.sample.features.backend.internal.FakeBackendImpl
import org.koin.dsl.module

/**
 * Created by Gonzalo Narbaiz on 17-4-21
 */

val backendModule = module {
    single { FakeBackendImpl() as BackendApi }
}