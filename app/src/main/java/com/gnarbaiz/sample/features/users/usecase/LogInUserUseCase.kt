package com.gnarbaiz.sample.features.users.usecase

import com.gnarbaiz.sample.features.common.usecase.UseCase
import com.gnarbaiz.sample.features.common.vo.catchResult
import com.gnarbaiz.sample.features.common.vo.Result
import com.gnarbaiz.sample.features.common.vo.onFailure
import com.gnarbaiz.sample.features.auth.AuthService
import com.gnarbaiz.sample.features.auth.di.APP_TOKEN
import com.gnarbaiz.sample.features.auth.di.USER_PIN
import com.gnarbaiz.sample.features.users.repository.LocalUsersRepository
import com.gnarbaiz.sample.features.users.repository.RemoteUserRepository
import com.gnarbaiz.sample.features.users.vo.User

typealias PinCode = CharArray

/**
 * Use case that attempts to log user in
 */
class LogInUserUseCase(
    private val localUsersRepository: LocalUsersRepository,
    private val remoteUserRepository: RemoteUserRepository,
    private val authService: AuthService
) :
    UseCase<CharArray, Result<User>> {

    override suspend fun invoke(param: PinCode) =
        catchResult {
            if (!tryOpenEncryptedDatabaseWith(param)) {
                throw WrongPasswordException()
            }
            val token = authService.getUserToken(APP_TOKEN)
            val user = remoteUserRepository.getPerson(token.accessToken)
            localUsersRepository.createUser(user)
            localUsersRepository.login(user)
            user
        }.onFailure {
            it.printStackTrace()
        }

    private fun tryOpenEncryptedDatabaseWith(param: PinCode): Boolean {
        // TODO: Instead of pin comparison, we should try opening the database with the pin
        // and not having the pin hardcoded in the app :)
        return param.contentEquals(USER_PIN)
    }
}

class WrongPasswordException : RuntimeException()