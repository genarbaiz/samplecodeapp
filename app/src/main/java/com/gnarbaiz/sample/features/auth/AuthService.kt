package com.gnarbaiz.sample.features.auth

import com.gnarbaiz.sample.features.auth.vo.Token

interface AuthService {
    suspend fun getUserToken(appToken: String): Token
}