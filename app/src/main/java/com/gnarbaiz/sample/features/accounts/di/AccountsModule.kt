package com.gnarbaiz.sample.features.accounts.di

import com.gnarbaiz.sample.features.accounts.repository.LocalAccountsRepository
import com.gnarbaiz.sample.features.accounts.repository.RemoteAccountsRepository
import com.gnarbaiz.sample.features.accounts.repository.internal.LocalAccountsRepositoryImpl
import com.gnarbaiz.sample.features.accounts.repository.internal.RemoteAccountsRepositoryImpl
import com.gnarbaiz.sample.features.accounts.ui.AccountListViewModel
import com.gnarbaiz.sample.features.accounts.usecase.FetchUserAccountsUseCase
import com.gnarbaiz.sample.features.accounts.usecase.RefreshUserAccountsUseCase
import com.gnarbaiz.sample.features.storage.database.AppDatabase
import com.gnarbaiz.sample.features.transactions.usecase.GetAccountBalanceUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Gonzalo Narbaiz on 17-4-21
 */

val accountsModule = module {
    viewModel { AccountListViewModel(get(), get()) }
    single { RefreshUserAccountsUseCase(get(), get(), get()) }
    single { FetchUserAccountsUseCase(get(), get(), get()) }
    single { RemoteAccountsRepositoryImpl(get(), get()) as RemoteAccountsRepository }
    single { LocalAccountsRepositoryImpl(get()) as LocalAccountsRepository }
    single { get<AppDatabase>().accountDao() }
    single { GetAccountBalanceUseCase(get(), get()) }
}