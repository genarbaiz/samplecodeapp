package com.gnarbaiz.sample.features.transactions.ui

import com.gnarbaiz.sample.features.accounts.repository.LocalAccountsRepository
import com.gnarbaiz.sample.features.accounts.vo.Account
import com.gnarbaiz.sample.features.common.usecase.UseCase
import com.gnarbaiz.sample.features.users.repository.LocalUsersRepository

/**
 * Returns bank accounts other than the currently selected one
 */
class GetAvailableAccountsForTransactionUseCase(
    private val usersRepository: LocalUsersRepository,
    private val localAccountsRepository: LocalAccountsRepository
) :
    UseCase<Account, List<Account>> {
    override suspend fun invoke(param: Account) =
        localAccountsRepository.getAccounts(usersRepository.getLoggedInUser()!!)
            .filter { it.id != param.id }
}