package com.gnarbaiz.sample.features.auth.vo

/**
 * Created by Gonzalo Narbaiz on 17-4-21
 */

data class Token(val accessToken: String,
                 val expireDate: Long)