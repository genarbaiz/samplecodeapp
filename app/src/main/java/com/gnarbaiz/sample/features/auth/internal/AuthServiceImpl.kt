package com.gnarbaiz.sample.features.auth.internal

import com.gnarbaiz.sample.features.auth.AuthService
import com.gnarbaiz.sample.features.auth.vo.Token
import com.gnarbaiz.sample.features.users.repository.RemoteUserRepository

class AuthServiceImpl(remoteUserRepository: RemoteUserRepository) :
    AuthService {

    private val getOrFetchUserToken =
        GetOrFetchUserTokenUseCase(TokenCacheFacade(remoteUserRepository))

    override suspend fun getUserToken(appToken: String): Token {
        return getOrFetchUserToken(appToken)
    }
}