package com.gnarbaiz.sample.features.storage.cache

/**
 * Created by Gonzalo Narbaiz on 17-4-21.
 */

/**
 * Representation of a cache.
 * A cache can be memory, disk, network source or whatever you can think of.
 */
interface Cache<Key : Any, Value : Any> {
    suspend fun get(key: Key): Value?
    suspend fun set(key: Key, value: Value)
    suspend fun evict(key: Key)
    suspend fun evictAll()
}

