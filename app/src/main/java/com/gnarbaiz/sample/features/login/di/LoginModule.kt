package com.gnarbaiz.sample.features.login.di

import com.gnarbaiz.sample.features.login.ui.LoginViewModel
import com.gnarbaiz.sample.features.users.usecase.LogInUserUseCase
import com.gnarbaiz.sample.features.users.usecase.LogOutUserUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Gonzalo Narbaiz on 17-4-21
 */

val loginModule = module {
    viewModel { LoginViewModel(get()) }
    single { LogInUserUseCase(get(), get(), get()) }
    single { LogOutUserUseCase(get()) }
}