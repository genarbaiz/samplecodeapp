package com.gnarbaiz.sample.features.auth.internal

import com.gnarbaiz.sample.features.auth.vo.Token
import com.gnarbaiz.sample.features.common.usecase.*

internal class GetOrFetchUserTokenUseCase(private val tokenCacheFacade: TokenCacheFacade) :
    UseCase<String, Token> {
    private val internalUseCase = object : UseCase<String, Token> {
        override suspend fun invoke(param: String): Token = tokenCacheFacade.cache.get(param)!!
    }.reuseInflight()

    override suspend fun invoke(param: String): Token = internalUseCase(param)!!
}