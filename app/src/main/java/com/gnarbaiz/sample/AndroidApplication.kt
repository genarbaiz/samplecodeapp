package com.gnarbaiz.sample

import android.app.Application

/**
 * Created by Gonzalo Narbaiz on 17-4-21.
 */

class AndroidApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        DependencyInjection.with(this)
    }
}

