package com.gnarbaiz.sample

import android.content.Context
import com.gnarbaiz.sample.features.accounts.di.accountsModule
import com.gnarbaiz.sample.features.auth.di.authModule
import com.gnarbaiz.sample.features.backend.di.backendModule
import com.gnarbaiz.sample.features.login.di.loginModule
import com.gnarbaiz.sample.features.storage.di.storageModule
import com.gnarbaiz.sample.features.transactions.di.transactionsModule
import com.gnarbaiz.sample.features.users.di.usersModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

// TODO: Split each feature in a separate gradle module

object DependencyInjection {
    fun with(applicationContext: Context) {
        startKoin {
            androidLogger()
            androidContext(applicationContext)
            modules(
                accountsModule,
                transactionsModule,
                usersModule,
                storageModule,
                authModule,
                backendModule,
                loginModule
            )
        }
    }
}